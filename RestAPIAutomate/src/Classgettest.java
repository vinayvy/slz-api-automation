import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import groovyjarjarasm.asm.commons.Method;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Classgettest extends baseapitest {
	static String  authtoken;
	static String custauthtoken;
	baseapitest baseobj = new baseapitest();
	List userids = new ArrayList();
	Properties prop = new Properties();
	InputStream input = null;
	static String zoneid;
	@Test
	public void GetRegionData() throws IOException
	{   
		/*ResourceBundle endpoint = ResourceBundle.getBundle("API_URL");
		RestAssured.baseURI = "https://qa-slz2.scholasticlearningzone.com/";*/
		String endpoint= baseobj.baseapi("regions");
		RequestSpecification httpRequest = RestAssured.given().auth().basic("EBB", "afb16a45-9c85-4b9d-9dee-cb7cd401f675");
		Response response = httpRequest.get(endpoint);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
 }
	
/*@SuppressWarnings("unchecked")
@BeforeClass
	public void customertokengenerate(){
	String endpoint= baseobj.baseapi("customerportallogin");
			RequestSpecification request = RestAssured.given();
			request.formParam("username", "RPattnaik-consultant@scholastic.com");
			request.formParam("password", "welcome1");
			request.header("Content-Type","application/x-www-form-urlencoded");
			Response response = request.post(endpoint);
			String responseBody = response.getBody().asString();
			System.out.println("Response Body is =>  " + responseBody);
			int statusCode = response.getStatusCode();
			Assert.assertEquals(statusCode actual value, 200 expected value, "Correct status code returned");
			if(statusCode == 200){
				custauthtoken = response.getBody().jsonPath().getString("authToken");
				System.out.println(custauthtoken);
			}
		}
@Test
public void CustPortalServiceZone(){
	String endpoint= baseobj.baseapi("regions");
	RequestSpecification httpRequest = RestAssured.given();
	Response response = httpRequest.get(endpoint);
	zoneid = response.jsonPath().getString("zoneId[0]");
    System.out.println(zoneid);
}
*/
@BeforeClass
	public void TokenGenerate(){
	String endpoint= baseobj.baseapi("loginauthenticate");
			RequestSpecification request = RestAssured.given();
			
			JSONObject requestParams = new JSONObject();
			requestParams.put("zoneCode", "SEE"); // Cast
			requestParams.put("siteId", "NRUF59J");
			requestParams.put("username", "sadmin1");
			requestParams.put("password", "welcome1");
			request.body("zoneCode=INTL&siteId=FJIDPHG&username=sadmin1&password=welcome1");
			Response response = request.post(endpoint);
			String responseBody = response.getBody().asString();
			System.out.println("Response Body is =>  " + responseBody);
			int statusCode = response.getStatusCode();
			Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
			if(statusCode == 200){
				authtoken = response.getBody().jsonPath().getString("authToken");
				System.out.println(authtoken);
			}
		}

	@SuppressWarnings("unchecked")
	@Test(priority = 1)
	public void CreateStudent(){
		String userid;
		String endpoint= baseobj.baseapi("createuser");
		 
		    Header h1 = new Header("Content-Type", "application/json");
		    Header h2 = new Header("auth-token", authtoken);
		    List<Header> list = new ArrayList<Header>();
		    list.add(h1);
		    list.add(h2);
		    Headers header = new Headers(list);
		 Date todaysDate = new Date();
			DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String str2 = df2.format(todaysDate);   
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		JSONObject requestParams = new JSONObject();
		String studentname = "student" + str2;
		requestParams.put("firstName", "studenttest9"); // Cast
		requestParams.put("lastName","studenttest9");
		requestParams.put("username", "studenttest9");
		requestParams.put("password", "welcome1");
		requestParams.put("districtUserId", "studenttest9");
		requestParams.put("yearGrade", "869");
		requestParams.put("type", "student");
		JSONObject requestsschool = new JSONObject();
		requestsschool.put("identifier", 3042);
		requestParams.put("school",requestsschool);
		request.body(requestParams.toJSONString());
		System.out.println(requestParams.toJSONString());
		Response response = request.post(endpoint);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		if(statusCode == 200){
			userid = response.getBody().jsonPath().getString("primaryKey");
			userids.add(userid);
			System.out.println(userids);
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Test(priority = 2)
 public void StudentSusbcription(){
		String endpoint= baseobj.baseapi("subscribeuser");
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		JSONObject requestParams = new JSONObject();
		List productids = new ArrayList();
		productids.add(45);
		requestParams.put("productIds", productids); // Cast
		requestParams.put("subscribe", true);
		requestParams.put("identifiers",userids);
		System.out.println(requestParams.toJSONString());
		request.body(requestParams.toJSONString());
		Response response = request.put(endpoint);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		
		
 }
	@SuppressWarnings("unchecked")
	@Test(priority = 3)
 public void StudentUnSusbcription(){
		String endpoint= baseobj.baseapi("unsubscribeuser");
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		JSONObject requestParams = new JSONObject();
		List productids = new ArrayList();
		productids.add(45);
		productids.add(44);
		requestParams.put("productIds", productids); // Cast
		requestParams.put("subscribe", false);
		requestParams.put("identifiers",userids);
		System.out.println(requestParams.toJSONString());
		request.body(requestParams.toJSONString());
		Response response = request.put(endpoint);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		
		
 }
	
	@Test(priority =4)
	public void deactivateuser(){
		String endpoint= baseobj.baseapi("deactivateuser");
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		JSONObject requestParams = new JSONObject();
		requestParams.put("users",userids);
		System.out.println(requestParams.toJSONString());
		request.body(requestParams.toJSONString());
		Response response = request.put(endpoint);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
	}
	@Test(priority =5)
	public void deleteeuser(){
		String endpoint= baseobj.baseapi("deleteuser");
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		JSONObject requestParams = new JSONObject();
		requestParams.put("users",userids);
		System.out.println(requestParams.toJSONString());
		request.body(requestParams.toJSONString());
		Response response = request.put(endpoint);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
	}
}
