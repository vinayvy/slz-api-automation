import java.util.ResourceBundle;

import org.testng.annotations.BeforeMethod;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class baseapitest {
	public String baseapi(String path){
	 RestAssured.baseURI = "https://qa-slz2.scholasticlearningzone.com/";		
	 ResourceBundle endpoint = ResourceBundle.getBundle("API_URL");
	return endpoint.getString(path);
	 
	}

}
