import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class StressPASdata {
	static String  authtoken;
	List userids = new ArrayList();
	@BeforeClass
	public void TokenGenerate(){
			RestAssured.baseURI = "https://stress-slz2.scholasticlearningzone.com/";
			RequestSpecification request = RestAssured.given();
			
			/*JSONObject requestParams = new JSONObject();
			requestParams.put("zoneCode", "SEE"); // Cast
			requestParams.put("siteId", "NRUF59J");
			requestParams.put("username", "sadmin1");
			requestParams.put("password", "welcome1");*/
			request.body("zoneCode=INTL&siteId=NZLF592&username=jennyb&password=welcome1");
			Response response = request.post("auth/api/authenticate");
			String responseBody = response.getBody().asString();
			System.out.println("Response Body is =>  " + responseBody);
			int statusCode = response.getStatusCode();
			Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
			if(statusCode == 200){
				authtoken = response.getBody().jsonPath().getString("authToken");
				System.out.println(authtoken);
			}
		}
	@Test(priority = 1)
	public void AllStudents(){
		String userid;
		RestAssured.baseURI = "https://stress-slz2.scholasticlearningzone.com/"; 
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		Response response = request.get("slz-portal/slzapi/v3/orgs/729/users/student");
		JSONObject requestParams = new JSONObject();
		List users = response.getBody().jsonPath().getJsonObject("users");
		System.out.println(users.size());
		for (int i=0; i<users.size(); i++) {
			 HashMap<String, String> hashmap= (HashMap<String, String>) users.get(i);
		      userid= hashmap.get("identifier");
		    /*JSONObject identifier = (JSONObject) users.get(i);
		    userid = (String) identifier.get("identifier");*/
		    System.out.println(userid);
		    userids.add(userid);
		}
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
	}
	@Test(priority = 2)
	public void AllTeacher(){
		String userid;
		RestAssured.baseURI = "https://stress-slz2.scholasticlearningzone.com/"; 
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		Response response = request.get("slz-portal/slzapi/v3/orgs/729/users/teacher");
		JSONObject requestParams = new JSONObject();
		List users = response.getBody().jsonPath().getJsonObject("users");
		System.out.println(users.size());
		for (int i=0; i<users.size(); i++) {
			 HashMap<String, String> hashmap= (HashMap<String, String>) users.get(i);
		      userid= hashmap.get("identifier");
		    /*JSONObject identifier = (JSONObject) users.get(i);
		     * 
		    userid = (String) identifier.get("identifier");*/
		    System.out.println(userid);
		    userids.add(userid);
		}
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
	}
	@Test(priority = 3)
	public void AllAdmin(){
		String userid;
		RestAssured.baseURI = "https://stress-slz2.scholasticlearningzone.com/"; 
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		Response response = request.get("slz-portal/slzapi/v3/orgs/729/users/administrator");
		JSONObject requestParams = new JSONObject();
		List users = response.getBody().jsonPath().getJsonObject("users");
		System.out.println(users.size());
		for (int i=0; i<users.size(); i++) {
			 HashMap<String, String> hashmap= (HashMap<String, String>) users.get(i);
		      userid= hashmap.get("identifier");
		    /*JSONObject identifier = (JSONObject) users.get(i);
		    userid = (String) identifier.get("identifier");*/
		    System.out.println(userid);
		    userids.add(userid);
		}
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
	}
@Test(priority = 4)
 public void StudentSusbcription(){
		RestAssured.baseURI = "https://stress-slz2.scholasticlearningzone.com/";
		RequestSpecification request = RestAssured.given().header("Content-Type", "Application/Json").header("auth-token", authtoken);
		JSONObject requestParams = new JSONObject();
		List productids = new ArrayList();
		productids.add(84);
		requestParams.put("productIds", productids); // Cast
		requestParams.put("subscribe", true);
		requestParams.put("identifiers",userids);
		System.out.println(requestParams.toJSONString());
		request.body(requestParams.toJSONString());
		Response response = request.put("slz-portal/slzapi/v3/orgs/729/subscriptions/users");
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		
		
 }
}
